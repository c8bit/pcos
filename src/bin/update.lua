
local currentPCSEVersion = settings.get("PCSE_VERSION", "0.0.0")

--[
-- function loadDependency
--   
--   Loads a dependency using "require", first from pcse lib, or from local
--   directory as a backup. This is necessary when 'update' is being run in
--   a minimal environment during initial installation.
--
--   Params:
--     <String> dependencyName: The name of the module to load
--
--   return <Table> or <nil>: The loaded module if found
--]
function loadDependency (dependencyName)
  local dep = nil

  -- Try loading from PCSE if it's installed
  local _libLoad = function ()
    local pcse = require("lib.pcse")
    if (pcse) then
      dep = pcse[dependencyName]
    end
    --print("Loaded from PCSE")
  end

  -- Try loading from local folder if we're bootstrapping
  local _lclLoad = function ()
    dep = require(dependencyName)
  end

  -- Load and check for errors
  if not pcall(_libLoad) and not pcall(_lclLoad) then
    print("ERROR: Failed to load dependency \""..dependencyName.."\".")
    dep = false
  end

  return dep
end

function loadDependencies()
  local success = true

  gitlab = loadDependency("gitlab")
  version = loadDependency("version")
  Dirs = loadDependency("project_dirs")

  local deps = {gitlab, Dirs, version}

  for _, dep in pairs(deps) do
    if not dep then
      print("Not all dependencies could be loaded. Aborting...")
      success = false
      break
    end
  end

  return success
end

--[
-- function checkNewVersions
--
--   Compares currently installed PCSE version against the newest version of
--   the repo.
--
--   return <Boolean>: True if a newer version is available, otherwise false
--]
function checkNewVersions()
  local currentVersion = version.Version.fromString(currentPCSEVersion)
  local newVersionAvailable

  if currentVersion:compareTo(pcseRepo:newestVersion()) < 0 then
    -- Newer version is available
    newVersionAvailable = pcseRepo:newestVersion()
  end
  
  return newVersionAvailable
end

function setNewVersion(newVersion)
  settings.set("PCSE_VERSION", newVersion)
end

function backupOldVersions()
  if fs.exists(Dirs.PROJECT_ROOT) then
    local currentVersion = version.Version.fromString(currentPCSEVersion)
    local newBackupDir = fs.combine(Dirs.PROJECT_OLDVERSIONS, currentVersion:toPath())
    fs.makeDir(newBackupDir)
    fs.move(Dirs.PROJECT_ROOT, newBackupDir)
  end
end

function downloadAndInstall()
  pcseRepo:cloneToLocalDir(Dirs.PROJECT_ROOT)
end

function linkStartupFile()
  -- Call PCSE startup file from user's startup file
  local defaultStartupFilename = "/startup.lua"
  local pcseStartupFilename = fs.combine(Dirs.LIB, "startup.lua")
  local startupNeedsLinking = false

  local linkingCodeComment = '\n--Run PCSE startup script\n'
  local linkingCode = 'shell.run("'..pcseStartupFilename..'")'

  if fs.exists(defaultStartupFilename) then
    local fileForReading = fs.open(defaultStartupFilename, "r")
    local nextLine

    repeat
      nextLine = fileForReading.readLine()
      if nextLine and string.find(nextLine, linkingCode) then
        startupNeedsLinking = true
        break
      end
    until(nextLine == nil)
    fileForReading.close()
  else
    startupNeedsLinking = true
  end

  if startupNeedsLinking then
    -- Append linking code
    local defaultStartupFile = fs.open(defaultStartupFilename, "a")
    defaultStartupFile.write(linkingCode)
    defaultStartupFile.close()
  end

end

-- Main entry ------------------------------------------
function main(args)
  local devMode = false
  local branch
  for _, value in ipairs(args) do
    if value == "-d" or value == "--develop" then
      devMode = true
      branch = "develop"
    end
  end

  print("Loading dependencies...")
  local depsLoaded = loadDependencies()

  if depsLoaded then
    print("Done.")

    pcseRepo = gitlab.GitlabRepository.pcseRepo(branch)

    local newVersion
    if not devMode then
      write("Checking PCSE version...")
      newVersion = checkNewVersions()
      print("Done.")
    end

    if newVersion then
      print("\n\nNew Version " .. newVersion:toString() .. " available, performing update.\n\n")

      -- Backup existing version if there is one
      write("Backing up old versions...")
      backupOldVersions()
      print("Done.")

      -- Download & Install files
      write("Downloading and installing files...")
      downloadAndInstall()
      print("Done.")

      write("Linking Startup script... ")
      linkStartupFile()
      shell.run("/startup.lua")
      print("Done.")

      setNewVersion(newVersion:toString())

      print("PCSE " .. newVersion:toString() .. " installed successfully!")
    elseif devMode then
      downloadAndInstall()
      print("Updated development codebase")
    else
      print("PCSE is up to date!")
    end


  end
end

main({...})

