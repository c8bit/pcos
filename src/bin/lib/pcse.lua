local libPath = "/usr/pcse/src/common/?.lua"
local libDevPath = "/usr/pcse-dev/src/common/?.lua"

package.path = package.path..";"..libDevPath..";"..libPath

return require("pcse")

