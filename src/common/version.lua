
Version = {}

-- Constructors

function Version:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self

  return o
end

function Version.fromString(versionString)
  self = Version:new()

  if versionString then
    self:setVersionString(versionString)
  end

  return self
end

-- Getters/Setters

function Version:setVersionString(versionString)
  -- Parse semantic version components
  local semanticPattern = string.find(versionString, "%d+[%._]%d+[%._]%d+")
  if semanticPattern then
    local semantic = string.sub(versionString, semanticPattern)

    self._major = tonumber(string.sub(semantic, string.find(semantic, "^%d+")))

    local vstart, vend, _ = string.find(semantic, "[%._](%d+)[%._]")
    self._minor = tonumber(string.sub(semantic, vstart + 1, vend - 1))

    self._hotfix = tonumber(string.sub(semantic, string.find(semantic, "%d+$")))
  end
end

function Version:toString()
  ret = nil
  if self._major and self._minor and self._hotfix then
    ret = self._major.."."..self._minor.."."..self._hotfix
  end

  return ret
end

function Version:toPath()
  local snake, _ = string.gsub(self:toString(), "%.", "_")
  return snake
end

function Version:compareTo(compVersion)
  local result = 0

  local _numCompare = function(a, b)
    if (a > b) then return 1; elseif (a < b) then return -1; else return 0; end
  end

  result = _numCompare(self._major, compVersion._major)

  if result == 0 then
    result = _numCompare(self._minor, compVersion._minor)
  end

  if result == 0 then
    result = _numCompare(self._hotfix, compVersion._hotfix)
  end

  return result
end

-- Return module map
return {
  Version = Version
}

