local PCSE_ROOT_DIR = "/usr/pcse"
local PCSE_OLDVERS_DIR = "/usr/pcse-oldversions"
local PCSE_DEV_ROOT_DIR = "/usr/pcse-dev"
local PCSE_CONFIG_DIR = fs.combine(PCSE_ROOT_DIR, "config")

local dirs = {
  PROJECT_ROOT = PCSE_ROOT_DIR,
  PROJECT_OLDVERSIONS = PCSE_OLDVERS_DIR,
  VERSION_CONFIG = fs.combine(PCSE_CONFIG_DIR, "version.cfg"),
  DEV_ROOT = PCSE_DEV_ROOT_DIR,
  LIB = fs.combine(PCSE_ROOT_DIR, fs.combine("src", "common")),
  BIN = fs.combine(PCSE_ROOT_DIR, fs.combine("src", "bin")),
  BOOT = fs.combine(PCSE_ROOT_DIR, fs.combine("src", "boot")),
  TEST = fs.combine(PCSE_ROOT_DIR, fs.combine("src", "test")),
}

return dirs
