local Dirs = require("project_dirs")

local versionConfig = fs.open(Dirs.VERSION_CONFIG, "r")
local configLine = versionConfig.readLine()
versionConfig.close()

if string.find(configLine, "PROJECT_VERSION.*%d+%.%d+%.%d+") then
  local currentVersion = string.sub(configLine, string.find(configLine, "%d+%.%d+%.%d+"))
  settings.set("PCSE_VERSION", currentVersion)
end


-- Add bins to path
if not string.find(shell.path(), Dirs.BIN) then
  shell.setPath(shell.path()..":"..Dirs.BIN)
end

shell.run("about_pcse --short")
