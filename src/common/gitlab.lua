version = require("version")

Version = version.Version

GitlabRepository = {}

-- Initializers ----------------------------------------
function GitlabRepository:new (o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self

  self._apiV4URL = "https://gitlab.com/api/v4/projects"
  self._cachedResults = {}
  self._params = {ref = "master"}

  return o
end

function GitlabRepository.repoWithID(gitlabID)
  self = GitlabRepository:new()
  self.gitlabID = gitlabID

  return self
end

function GitlabRepository.pcseRepo(branch)
  local repo = GitlabRepository.repoWithID("35368954")
  if branch then
    repo._params["ref"] = branch
  return repo
end

-- Getters ---------------------------------------------

function GitlabRepository:getProjectURL()
  return self._apiV4URL .. "/" .. self.gitlabID
end

function GitlabRepository:getTagsURL()
  return self:getProjectURL() .. "/repository/tags"
end

function GitlabRepository:getFileTreeURL()
  return self:getProjectURL() .. "/repository/tree"
end

function GitlabRepository:getRawFileURL(encodedPath)
  return self:getProjectURL() .. "/repository/files/" .. encodedPath .. "/raw"
end

function GitlabRepository:getTags()
  if not self._cachedResults["tags"] then
    local tagsCall = function()
      local request = http.get(self:getTagsURL())
      local result = request.readAll()
      request.close()

      return result
    end

    local requestResult = tagsCall()
    local json = textutils.unserializeJSON(requestResult)

    self._cachedResults["tags"] = json
  end

  return self._cachedResults["tags"]
end

function GitlabRepository:downloadRawFile(repoPath, lclFilePath)
  local encodedTreePath = self:encodeTreePath(repoPath)
  local file = fs.open(lclFilePath, "w")
  local request = http.get(self:getRawFileURL(encodedTreePath) .. self:encodedParams())
  file.write(request.readAll())
  file.close()
  request.close()
end

-- recursive method for walking through and downloading files
function GitlabRepository:_cloneAtPath(cloneDirectory, path)
  print("Cloning at path "..path)
  -- https request for the API
  local params
  if path then
    params = {path = path}
  end
  print("Req: ".. self:getFileTreeURL() .. self:encodedParams(params))
  local request = http.get(self:getFileTreeURL() .. self:encodedParams(params))
  local result = request.readAll()
  request.close()


  local tree = textutils.unserializeJSON(result)

  -- clone the file or folder
  for _, fileItem in ipairs(tree) do
    print("First item "..fileItem["name"])
    local lclDir = fs.combine(cloneDirectory, fs.combine(path, fileItem["name"]))
    if fileItem["type"] == "tree" then
      -- Found a folder, make the folder and clone recursively
      print("Making directory "..lclDir)
      fs.makeDir(lclDir)
      self:_cloneAtPath(cloneDirectory, fs.combine(path, fileItem["name"]))

    elseif fileItem["type"] == "blob" then
      -- Found a file, download it to the current local directory
      print("Downloading file: "..lclDir)
      self:downloadRawFile(fs.combine(path, fileItem["name"]), lclDir)
    end
  end
end

function GitlabRepository:cloneToLocalDir(cloneDirectory)

  --local requestResult = filetreeCall()
  self:_cloneAtPath(cloneDirectory, "")

end

function GitlabRepository:getTagNames()

  local allTags = {}
  for _, tag in ipairs(self:getTags()) do
    table.insert(allTags, tag["name"])
  end

  return allTags
end

function GitlabRepository:newestVersion()
  local version = Version.fromString("0.0.0")

  for _, tagname in ipairs(self:getTagNames()) do
    local nextVersion = Version.fromString(tagname)
    if version:compareTo(nextVersion) < 0 then
      version = nextVersion
    end
  end

  return version
end

-- Encoding & request parameters -------------------------------

function GitlabRepository:encodeTreePath(treePath)
  return string.gsub(treePath, "/", "%%2F")
end

function GitlabRepository:encodedParams(extraParams)
  local allParams = self._params
  local paramString = "?"
  local firstParam = true

  -- Combine default Params with extra params
  if extraParams then
    for key, val in pairs(extraParams) do
      allParams[key] = val
    end
  end

  for key, value in pairs(allParams) do
    if not firstParam then paramString = paramString .. '&' end
    firstParam = false

    paramString = paramString .. key .. '=' .. value
  end

  return paramString
end

-- Other methods -------------------------------------------

function GitlabRepository:flush()
  self._cachedResults = {}
end

-- Return module map
return {
  GitlabRepository = GitlabRepository
}
