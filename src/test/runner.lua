Runner = {}

function Runner:new(o)
  o = o or {}
  setmetatable(o, self)
  self.__index = self

  return o
end

function Runner:startup()
--TODO: Can use this to set up a /tmp dir for integration/downloading tests, terdown to cleanup
end

function Runner:runAll()
  self:startup()

  local successCount = 0
  local failureCount = 0
  local failuresList = {}

  local cwd = fs.getDir(shell.getRunningProgram())
  local testfiles = fs.find(fs.combine(cwd, "*_tests.lua"))

  for _, filename in ipairs(testfiles) do
    -- Remove '.lua' extension
    local modulename = string.sub(fs.getName(filename), 1, -5)

    local testclass = require(modulename)
    local t = testclass.TestClass:new()
    local successes, failures, failureTable = t:runTests()

    successCount = successCount + successes
    failureCount = failureCount + failures
    if next(failureTable) ~= nil then
      failuresList[t.name] = failureTable
    end
  end

  self:reportResults(successCount, failureCount, failuresList)

  self:teardown()
end

function Runner:reportResults(successCount, failureCount, failures)
  print("Tests completed!")
  print("  "..successCount.." tests passed, "..failureCount.." failed.")
  print("")

  for filename, msgTable in pairs(failures) do
    print("In "..filename..":")

    for methodName, msg in pairs(msgTable) do
      print("  "..methodName..": "..msg)
      print("")
    end
  end
end

function Runner:teardown()

end

-- Return module map
return {
  Runner = Runner
}


