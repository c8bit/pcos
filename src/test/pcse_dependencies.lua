local libPath = "/usr/pcse/common/?.lua"
local libDevPath = "/usr/pcse-dev/common/?.lua"

package.path = package.path..";"..libDevPath..";"..libPath

return require("pcse")
