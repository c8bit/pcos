local testCase = require("testcase")
local pcse = require("pcse_dependencies")

local TestCase = testCase.TestCase
local Version = pcse.version.Version


local VersionTests = {}

function VersionTests:new()
  VersionTests.__index = VersionTests
  setmetatable(VersionTests, {__index = TestCase})
  local obj = TestCase:new("version_tests")
  setmetatable(obj, VersionTests)

  return obj
end

function VersionTests:setup()
  TestCase.setup(self)

  self.versionStrings = {
    "0.0.0",
    "1.2.3",
    "99999.0.1",
    "0.98765.2",
    "1.2.12345",
    "12345.23456.34567",
  }

  self.badVersionStrings = {
    "1.2",
    "7",
    "v9",
    "2534635",
    "25434353.216435",
    "foobar",
    "hello, world",
    "",
  }

end

function VersionTests:test_toString()
  for _, versionString in pairs(self.versionStrings) do
    local vsn = Version.fromString(versionString)

    assert(vsn:toString() == versionString, versionString .. " not equal to " .. (vsn:toString() or "<nil>"))
  end
end

function VersionTests:test_negative_toString()
  for _, versionString in pairs(self.badVersionStrings) do
    local vsn = Version.fromString(versionString)

    assert(not vsn:toString(), "Version string for param "..versionString.." is non-nil")
  end
end

function VersionTests:test_compareTo()
  local comparisons = {
    {"0.0.1", "0.0.0",  1},
    {"0.1.0", "0.0.0",  1},
    {"1.0.0", "0.0.0",  1},

    {"0.0.0", "0.0.1", -1},
    {"0.0.0", "0.1.0", -1},
    {"0.0.0", "1.0.0", -1},

    {"1.4.0", "1.3.26",    1},
    {"2.0.0", "1.987.65",  1},

    {"1.3.26", "1.4.0",   -1},
    {"1.987.65", "2.0.0", -1},

    {"123.4.567", "123.4.56",    1},
    {"123.40.567", "123.4.567",  1},

    {"123.4.56", "123.4.567",   -1},
    {"123.4.567", "123.40.567", -1},
  }

  for _, comparison in ipairs(comparisons) do
    local baseVerString, compVerString, result = unpack(comparison)
    local ver = Version.fromString(baseVerString)
    local computedResult = ver:compareTo(Version.fromString(compVerString))
    assert(
       computedResult == result,
      "Incorrect result ("..computedResult..") comparing "..baseVerString.." to "..compVerString
    )
  end

  local singleValueComparisons = {
    "0.0.0",
    "1.0.1",
    "123.4.5678",
    "12345.678.0",
  }

  for _, versionString in ipairs(singleValueComparisons) do
    local ver = Version.fromString(versionString)
    assert(
      ver:compareTo(Version.fromString(versionString)) == 0,
      "Comparison between "..versionString.." and itself should be 0"
    )
  end
end

function VersionTests:teardown()
  TestCase.teardown(self)
end

-- Return module map
return {
  TestClass = VersionTests
}

