TestCase = {}

function TestCase:new(name)
  TestCase.__index = TestCase
  local obj = {}
  setmetatable(obj, TestCase)

  self.name = name

  return obj
end

function TestCase:setup()
  print("base setup")
end

function TestCase:runTests()
  local failed = {}
  local successCount = 0
  local failureCount = 0

  self:setup()

  for itemName, func in pairs(getmetatable(self)) do
    if string.find(itemName, "^test_") then
      print("Running "..itemName)

      local result, data = pcall(func, self)
      if result then
        successCount = successCount + 1
      else
        print("Test failed!")
	failed[itemName] = data
	failureCount = failureCount + 1
      end
    end
  end

  self:teardown()

  return successCount, failureCount, failed
end

function TestCase:teardown ()

end


-- Return module map
return {
  TestCase = TestCase
}



