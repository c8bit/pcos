local TEMP_DIR = "/tmp"

function bootstrapInstallDependencies(branch)
  -- Make temp Dir
  if not fs.exists(TEMP_DIR) then
      fs.makeDir(TEMP_DIR)
  end

  -- Collect & install dependencies to run 'update'
  deps = {
    "src/common/gitlab.lua",
    "src/common/version.lua",
    "src/common/project_dirs.lua",
    "src/bin/update.lua",
  }

  local gitlabURLPrefix = "https://gitlab.com/api/v4/projects/35368954/repository/files/"
  local gitlabURLSuffix = "/raw?ref="..branch

  -- TODO: Error handling on http request
  for index, depName in pairs(deps) do
    -- Gitlab request
    local encodedDepPath = string.gsub(depName, "/", "%%2F")
    local gitlabRequestURL = gitlabURLPrefix..encodedDepPath..gitlabURLSuffix
    local gitlabRequest = http.get(gitlabRequestURL)

    -- File output
    local fileBasename = string.sub(depName, string.find(depName, "[%a_%d]+.lua"))
    local file = fs.open(TEMP_DIR.."/"..fileBasename, "w")
    file.write(gitlabRequest.readAll())

    file.close()
    gitlabRequest.close()
  end

end

function cleanup()
  fs.delete(TEMP_DIR)
end

function main(args)
  local branch = "master"
  local options = ""
  for _, value in ipairs(args) do
    if value == "-d" or value == "--develop" then
        branch = "develop"
        options = "-d"
    end
  end

  print("Collecting dependencies...")
  bootstrapInstallDependencies(branch)
  print("Done.")
  
  print("Launching update script...")
  shell.run(TEMP_DIR.."/update.lua", options)
  print("Done.")
  
  print("Cleaning up...")
  cleanup()
  print("Done.")
end

main({...})





