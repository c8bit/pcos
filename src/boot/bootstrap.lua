url = "https://gitlab.com/api/v4/projects/35368954/repository/files/src%2Fboot%2Finstall.lua/raw?ref=master"
request = http.get(url)
installPath = "/boot/install.lua"

file = fs.open(installPath, "w")
file.write(request.readAll())

-- Cleanup
file.close()
request.close()

shell.run(installPath)

